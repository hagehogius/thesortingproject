package com.practice;

import org.apache.log4j.Logger;

public class App{
    static Logger logger = Logger.getLogger(App.class);

    public static void main(String[] args) {

        logger.trace("Starting application.");

        String input = new String(DataInOut.enterArray(null));
        logger.trace("Entered array: " + input);
        int[] result = Sorting.sort(input);
        DataInOut.printResult(result);

        logger.trace("Terminating application.");

    }

}
