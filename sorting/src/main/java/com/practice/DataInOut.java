package com.practice;

import java.util.Scanner;

import org.apache.log4j.Logger;

public class DataInOut {

//    static Logger logger = Logger.getLogger(DataInOut.class);
    static Logger logger = Logger.getLogger(DataInOut.class);

    public static String enterArray(String array) {

        String input;

        System.out.print("Enter an array of integer values up to 10 elements:");
        Scanner scanner = new Scanner(System.in);

        if (array == null) {
            input = scanner.nextLine();
        } else {
            input = array;
        }
        input=input.replace(","," ");
        input=input.replace(", "," ");

        // if count of elements is more than 10
        try {
            if (input.split(" ").length > 10) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ex) {
            logger.error("Input array contains more than 10 elements");
            throw new IllegalArgumentException("Entered array has more than 10 elements");
        }

        // if there is no integer element
        try {
            if (input.contains(".") == true) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException ex) {
            logger.error("All elements must be integer");
            throw new IllegalArgumentException("All elements must be integer");
        }

        return input;
    }

    public static void printResult(int[] result) {

        StringBuilder sb = new StringBuilder();
        int i=0;
        while (i<result.length) {
            sb.append(result[i] + " ");
            i++;
        }
        logger.trace("Sorted array: " + sb.toString());
    }
}
