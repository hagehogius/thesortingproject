package com.practice;

public class Sorting {

    public static int[] sort(String input) {
        int sizeOfArray = 0;

        //transform string array to int array
        String[] array = input.split(" ");
        try {
            Integer.parseInt(array[0]);
            sizeOfArray = array.length;
        } catch (NumberFormatException e) {
            sizeOfArray = 0;
        }
        int[] arrayInt = new int[sizeOfArray];
        for (int i = 0; i < arrayInt.length; i++) {
            arrayInt[i] = Integer.parseInt(array[i]);
        }

        //sorting
        int shelf; //variable for temporary memorise value of swapping element
        for (int i = 0; i < arrayInt.length; i++) { //size of working array, every turn it becomes less
            int minValue = arrayInt[i];
            int minValuePosition = i;
            for (int b = i + 1; b < arrayInt.length; b++) { //searching fewest value
                if (arrayInt[b] < minValue) {
                    minValue = arrayInt[b];
                    minValuePosition = b;
                }
            }
            //swapping elements
            shelf = arrayInt[i];
            arrayInt[i] = minValue;
            arrayInt[minValuePosition] = shelf;
        }

        return arrayInt;
    }
}
