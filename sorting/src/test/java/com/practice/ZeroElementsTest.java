package com.practice;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class ZeroElementsTest {
    Sorting sorting = new Sorting();
    @Test
    public void zeroElementsTest() {
        assertEquals("[]", Arrays.toString(sorting.sort("")));


    }

}
