package com.practice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class OneElementTest {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"1"},
                {"0"},
                {"-1"}
                });
    }

    Sorting sorting = new Sorting();

    private String input;

    public OneElementTest(String input) {
        this.input = input;
    }

    @Test
    public void oneElementTest () {
        assertEquals(Arrays.toString(input.split(" ")), Arrays.toString(sorting.sort(input)));
    }
}
