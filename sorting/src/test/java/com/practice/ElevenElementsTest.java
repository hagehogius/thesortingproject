package com.practice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ElevenElementsTest {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"1 2 3 4 5 6 7 8 9 10 11"},
                {"11 10 9 8 7 6 5 4 3 2 1"},
                {"-5 -4 -3 -2 -1 0 1 2 3 4 5"},
                {"-3 3 -1 1 -5 5 0 2 -2 -4 4"}
        });
    }

    DataInOut sorting = new DataInOut();

    private String input;

    public ElevenElementsTest(String input) {
        this.input = input;
    }

    @Test (expected = IllegalArgumentException.class)
    public void ElevenElementsTest () {
        DataInOut.enterArray(input);
    }
}
