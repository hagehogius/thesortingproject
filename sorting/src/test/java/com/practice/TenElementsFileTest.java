package com.practice;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class TenElementsFileTest {

    Sorting sorting = new Sorting();

    @Test
    @FileParameters("src/test/resources/arrays.csv")
    public void TenElementsTest (String input) {
        int[] expected = Arrays.stream(input.split(" ")).mapToInt(Integer::parseInt).toArray();
        Arrays.sort(expected);
        assertEquals(Arrays.toString(expected), Arrays.toString(sorting.sort(input)));
    }
}
